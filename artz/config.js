import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyAeApcuSdf6KuHQMnC0aahxEVW3w91OZsM",
  authDomain: "artz-bacdd.firebaseapp.com",
  projectId: "artz-bacdd",
  storageBucket: "artz-bacdd.appspot.com",
  messagingSenderId: "638155960353",
  appId: "1:638155960353:web:5e038b534bc0dd067ef360",
  measurementId: "G-FQMQWP54DZ"
};
  

  if(!firebase.apps.length)
  {
    firebase.initializeApp(firebaseConfig)
  }

  export {firebase};