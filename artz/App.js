import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import { Login, Signup, Welcome } from "./screens/Index";
import { useEffect, useState } from 'react';
import {firebase} from './config'
import EditProfileView from './screens/EditProfile';
import Tabs from './screens/navaigation/tabs';
const Stack = createStackNavigator();
import Doctors from './Doctors';
import DoctorBooking from './screens/Doctorbooking';
import Confirmbooking from './screens/Confirmbooking';
import ProfileScreen from './screens/ProfileScreen';
import TabsDoctor from './screens/navaigation/tabsDoctor';
import Dash from './screens/Dash';
import SignoutPatients from './screens/SignoutPatients';
import UpdateAppointment from './screens/UpdateAppointment';
 function App() {
  
  const [initializing,setInitialazing] = useState(true);
  const [user,setUser] = useState();
  const [iniRouteTab , setiniRouteTab] = useState('');
  function onAuthStateChanged(user){
    setUser(user);
    if(initializing) setInitialazing(false);
  }



  useEffect( ()=> {
    const suscriber = firebase.auth().onAuthStateChanged(onAuthStateChanged);
    return suscriber;
  } , [user])

  if(initializing) return null;

  if(!user){
    return(
      <Stack.Navigator
        initialRouteName='Welcome'
      >
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="SignoutPatients"
          component={SignoutPatients}
          options={{
            headerShown: false
          }}
        />
      </Stack.Navigator>
    )
  }
  return(
    <Stack.Navigator
            initialRouteName="Dash"
        >
        <Stack.Screen
              name="Dash"
              component={Dash}
              options={{
                headerShown: false,
              }}
            />
        <Stack.Screen
              name="TabsDoctor"
              component={TabsDoctor}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Tabs"
              component={Tabs}
              options={{
                headerShown: false,
              }}
            />
             <Stack.Screen
              name="UpdateAppointment"
              component={UpdateAppointment}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="ProfileScreen"
              component={ProfileScreen}
              options={{
                headerShown: false
              }}
            />
            <Stack.Screen
              name="EditProfile"
              component={EditProfileView}
              options={{
                headerShown: false
              }}
            />
            <Stack.Screen
              name="Doctors"
              component={Doctors}
              options={{
                headerShown: false
              }}
            />
            <Stack.Screen
              name="Confirmbooking"
              component={Confirmbooking}
              options={{
                headerShown: false
              }}
            />
            <Stack.Screen
              name="DoctorBooking"
              component={DoctorBooking}
              options={{
                headerShown: false
              }}
            />
        </Stack.Navigator>
  )
}
export default () => {
  return(
    <NavigationContainer>
      <App/>
    </NavigationContainer>
  )
}
