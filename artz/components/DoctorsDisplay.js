import { StyleSheet, Text, View, Image, Animated, Pressable , TextInput , FlatList } from "react-native";
import React from "react";
import { StatusBar } from 'expo-status-bar';
import { MaterialIcons } from '@expo/vector-icons'; 
import { useState,useEffect } from "react";
import {firebase} from '../config';
import { Feather } from "@expo/vector-icons";
import { AntDesign } from '@expo/vector-icons'; 
const DoctorsDisplay = (props) => {
  const doctors = props.item;
  console.log(doctors)
  const userAddress = props.userAddress;
  const [filteredDoctors, setFilteredDoctors] = useState([]);
  const [input, setInput] = useState("");
  const [userInfo, setUserinfo] = useState('');
    useEffect( ()=> {
        firebase.firestore().collection('patients')
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());            }
            else{
                console.log('User does not exist');
            }
        })
      } , [])

      const handleSearch = (text) => {
        setInput(text.trim());
        const filtered = doctors.filter((item) => {
          const { firstname, lastname, location, speciality } = item;
      
          // Check if the properties exist and are not undefined
          const fName = firstname ?? '';
          const lName = lastname ?? '';
          const loc = location ?? '';
          const specs = Array.isArray(speciality) ? speciality : [];
      
          return (
            fName.toLowerCase().includes(text.trim().toLowerCase()) ||
            lName.toLowerCase().includes(text.trim().toLowerCase()) ||
            loc.toLowerCase().includes(text.trim().toLowerCase()) ||
            specs.some((spec) => spec.toLowerCase().includes(text.trim().toLowerCase()))
          );
        });
        setFilteredDoctors(filtered);
      };
      
      const renderEmptyResult = () => {
        if (input.trim() !== '' && filteredDoctors.length === 0) {
          return (
            <View style={{ alignItems: 'center', marginTop: 20 , marginHorizontal:8}}>
              <View style={{ alignItems: 'center', flex:1 ,flexDirection:'row'} } >
                <AntDesign name="find" size={24} color="green" />
                <Text style={{fontSize:20 , fontWeight:"500", marginTop:2 , marginLeft:8}}>No result found for this search</Text>
              </View>
              
              <Text style={{fontSize:16 , fontWeight:"100" , marginTop:10}}>Remmember , you can only search for location (city) , names 
              and specialities 😁</Text>
            </View>
          );
        }
        return null;
      };
  const AVATAR_SIZE = 70;
  const SPACING = 20;

    // console.log(props.item);

  return (
    <View>
      <View
          style={{
              padding: 10,
              margin: 10,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              borderWidth: 0.8,
              borderColor: "#C0C0C0",
              borderRadius: 7,
              
               zIndex:999,
              backgroundColor:"white"
          }}
          >
          <TextInput 
            style={{flex: 1}}
            onChangeText={handleSearch}
            value={input}
            placeholder="Search for items or More" />
          <Feather name="search" size={24} color="#007260"/>
        </View>

      <FlatList
        data={input.trim() === '' ? doctors : filteredDoctors}
        keyExtractor={(item) => item.email}
        contentContainerStyle={{
          padding: SPACING,
          paddingTop: StatusBar.currentHeight || 42,
        }}
        renderItem={({ item, index }) => {
          
          const specialities = Array.isArray(item.speciality) ? item.speciality : [];
          return (
            <View
              style={{
                flexDirection: "row",
                padding: SPACING,
                marginBottom: SPACING,
                backgroundColor: "rgba(255,255,255,0.9)",
                borderRadius: 12,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 10,
                },
                shadowOpacity: 0.3,
                shadowRadius: 20,
              }}
            >
              <Pressable onPress={() => props.navigation.navigate("DoctorBooking", {doctor:item , patient:userInfo , uAddress:{userAddress} })}>
                <Image
                  source={{ uri: item.image }}
                  style={{
                    width: AVATAR_SIZE,
                    height: AVATAR_SIZE,
                    borderRadius: AVATAR_SIZE,
                    marginRight: SPACING / 2,
                  }}
                />
                <View>
                  <Text style={{ fontSize: 22, fontWeight: "700" , marginBottom:8 }}>
                    {item.firstname} {item.lastname}
                  </Text>
                  <Text style={{ fontSize: 15, opacity: 0.7  , marginBottom:8}}>
                      {specialities.map((speciality, index) => (
                        <Text key={index}>{speciality} </Text>
                      ))}
                  </Text>
                  
                  <View style={{ flex: 1, flexDirection: 'row', paddingTop: 2  , marginBottom:8}}>
                    <MaterialIcons name="contact-mail" size={18} color="green" />
                    <Text style={{ fontSize: 14, opacity: 0.8, color: "black", paddingLeft: 6 }}>{item.email}</Text>
                  </View>
                </View>
              </Pressable>
            </View>
          );
        }}
      />
      {renderEmptyResult()}
    </View>
  );
};

export default DoctorsDisplay;

const styles = StyleSheet.create({});
