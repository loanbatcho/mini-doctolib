import React, { createContext, useState } from 'react';

// Create the RefreshContext
export const RefreshContext = createContext();

// Create a RefreshProvider component to wrap your app
export const RefreshProvider = ({ children }) => {
  const [refreshState, setRefreshState] = useState(false);

  // Function to toggle the refresh state
  const toggleRefreshState = () => {
    setRefreshState(!refreshState);
  };

  // Pass the refreshState and toggleRefreshState to the context provider value
  const contextValue = {
    refreshState,
    toggleRefreshState,
  };

  return (
    <RefreshContext.Provider value={contextValue}>
      {children}
    </RefreshContext.Provider>
  );
};
