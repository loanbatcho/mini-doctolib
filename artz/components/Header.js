import {
    StyleSheet,
    Text,
    View,
    Alert,
    Pressable,
    Image,
  } from 'react-native';
  import React, { useEffect, useState  , useContext} from 'react';
  import * as Location from 'expo-location';
  import { MaterialIcons } from '@expo/vector-icons';
  import {firebase} from '../config'
import { useNavigation } from '@react-navigation/native';

  const Header = (props) => {

    const collection = (String) (props.collection);
    const navigation=useNavigation();
    const [userInfo, setUserInfo] = useState('');
    useEffect(() => {
        firebase
        .firestore()
        .collection(collection)
        .doc(firebase.auth().currentUser.uid)
        .get()
        .then((snapshot) => {
            if (snapshot.exists) {
            setUserInfo(snapshot.data());
            } else {
            console.log('User does not exist');
            }
        });
    }, []);
  
    const [displayCurrentAddress, setDisplayCurrentAddress] = useState(
      'we are loading your location'
    );
    const [locationServicesEnabled, setLocationServicesEnabled] = useState(
      false
    );
  
    useEffect(() => {
      checkIfLocationEnabled();
      getCurrentLocation();
    }, []);
  
    const checkIfLocationEnabled = async () => {
      let enabled = await Location.hasServicesEnabledAsync();
      if (!enabled) {
        Alert.alert(
          'Location services not enabled',
          'Please enable the location services',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        );
      } else {
        setLocationServicesEnabled(enabled);
      }
    };
  
    const getCurrentLocation = async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
  
      if (status !== 'granted') {
        Alert.alert(
          'Permission denied',
          'Allow the app to use the location services',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        );
      }
  
      const { coords } = await Location.getCurrentPositionAsync();
      if (coords) {
        const { latitude, longitude } = coords;
  
        let response = await Location.reverseGeocodeAsync({
          latitude,
          longitude,
        });
  
        for (let item of response) {
          let address = `${item.name} ${item.city} ${item.postalCode}`;
          setDisplayCurrentAddress(address);
        }
      }
    };
  
    return (

        <View style={{ backgroundColor: '#F0F0F0', flex: 1, marginTop: 50 , borderBottomColor : "#007260" ,  shadowColor: '#000',
          shadowOffset: {
            width: 5,
            height: 5,
          },borderBottomWidth:1}}>
          {/* Location and Profile */}
          
          <View
            style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}
          >
            <MaterialIcons name="location-on" size={30} color="#007260" />
            <View>
              <Text style={{ fontSize: 18, fontWeight: '600' }}>Home</Text>
              <Text>{displayCurrentAddress}</Text>
            </View>
  
            <Pressable
              onPress={() => navigation.navigate('ProfileScreen' , {collection:collection})}
              style={{ marginLeft: 'auto', marginRight: 7 }}
            >
              <Image
                style={{ width: 40, height: 40, borderRadius: 20 }}
                source={{
                  uri: userInfo.image,
                }}
              />
            </Pressable>
          </View>
        </View>

    );
  };
  
  export default Header;
  