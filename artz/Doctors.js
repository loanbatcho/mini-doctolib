import React, { useEffect, useState } from 'react';
import { View, Text, Image, FlatList, Animated, StyleSheet, TouchableOpacity , SafeAreaView , TextInput } from 'react-native';
import { firebase } from './config';
import { Feather } from "@expo/vector-icons";
import { StatusBar } from 'expo-status-bar';
import Search from './screens/Search';
import { useRoute } from "@react-navigation/native";
import { MaterialIcons } from '@expo/vector-icons'; 
import { Pressable } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Button from './components/Button';
import { Searchbar } from 'react-native-paper';

const Doctors = (props) => {
  const [filteredDoctors, setFilteredDoctors] = useState([]);
  const [input, setInput] = useState("");
  const navigation = useNavigation();
  const [matchDoctors, setMatchDoctors] = useState([]);
  const route = useRoute();
  const specialityWanted = route.params.speciality;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const snapshot = await firebase.firestore().collection('Doctors').get();
        const data = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        const matchDoctors = data.filter((doctor) =>
          doctor.speciality && doctor.speciality.some((speciality) =>
            speciality.toLowerCase().startsWith(specialityWanted.toLowerCase().slice(0, 5))
          )
        );
        setMatchDoctors(matchDoctors);
      } catch (error) {
        console.log('Error getting doctors:', error);
      }
    };
  
    fetchData();
  }, [specialityWanted]);
  
  
  const handleSearch = (text) => {
    setInput(text);
    const filtered = matchDoctors.filter(
      (item) =>
        item.firstname.toLowerCase().includes(text.toLowerCase()) ||
        item.lastname.toLowerCase().includes(text.toLowerCase()) ||
        item.speciality.some((speciality) =>
          speciality.toLowerCase().includes(text.toLowerCase())
        )
    );
    setFilteredDoctors(filtered);
  };

  const [userInfo, setUserinfo] = useState('');

  useEffect(() => {
    firebase.firestore().collection('patients')
      .doc(firebase.auth().currentUser.uid)
      .get()
      .then((snapshot) => {
        if (snapshot.exists) {
          setUserinfo(snapshot.data());
        } else {
          console.log('User does not exist');
        }
      });
  }, []);

  const AVATAR_SIZE = 70;
  const SPACING = 20;
  return (
    <SafeAreaView style={styles.container}>
     
        <View style={{padding:5, marginLeft:10 ,marginTop:25}}>
          <Text style={{ fontSize: 20,  marginBottom: 8 , fontWeight:400 , color:'green' }}>
              Service: {specialityWanted}
          </Text>
        </View>
        <View
          style={{
              padding: 10,
              margin: 10,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              borderWidth: 0.8,
              borderColor: "#C0C0C0",
              borderRadius: 7,
                top: 0 ,
               zIndex:999,
              backgroundColor:"white"
          }}
          >
          <TextInput 
            style={{flex: 1}}
            placeholder="Search"
          onChangeText={handleSearch}
          value={input}/>
          <Feather name="search" size={24} color="#007260"/>
        </View>
      <FlatList
        data={input.trim() === '' ? matchDoctors : filteredDoctors}
        keyExtractor={(item) => item.email}
        contentContainerStyle={{
          padding: SPACING,
          paddingTop: StatusBar.currentHeight || 42,
        }}
        renderItem={({ item, index }) => (
          <View> 
          
          <View
            style={{
              flexDirection: 'row',
              padding: SPACING,
              marginBottom: SPACING,
              backgroundColor: 'rgba(255,255,255,0.9)',
              borderRadius: 12,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 10,
              },
              shadowOpacity: 0.3,
              shadowRadius: 20,
            }}
          >
            
            <Pressable onPress={() => navigation.navigate("DoctorBooking", { doctor: item, patient: userInfo })}>
              <Image
                source={{ uri: item.image }}
                style={{
                  width: AVATAR_SIZE,
                  height: AVATAR_SIZE,
                  borderRadius: AVATAR_SIZE,
                  marginRight: SPACING / 2,
                }}
              />
              <View>
                <Text style={{ fontSize: 17, fontWeight: "700", marginBottom: 8 }}>
                  {item.firstname} {item.lastname}
                </Text>
                
                <View style={{ flex: 1, flexDirection: 'row', paddingTop: 2, marginBottom: 8 }}>
                  <MaterialIcons name="contact-mail" size={18} color="green" />
                  <Text style={{ fontSize: 14, opacity: 0.8, color: "black", paddingLeft: 6 }}>{item.email}</Text>
                </View>
              </View>
            </Pressable>
          </View>
          </View>
        )}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F0F0',
  },
  backButton: {
    padding: 4,
  },
  searchContainer: {
    position: 'sticky',
    top: 0,
    zIndex: 999,
    backgroundColor: '#F0F0F0',
  },
  searchBar: {
    marginTop: 12,
  },
});

export default Doctors;
