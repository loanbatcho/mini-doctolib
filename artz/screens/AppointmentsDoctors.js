import { View, Text, StyleSheet, Dimensions, Image, FlatList, ScrollView , Pressable} from 'react-native';
import { firebase } from '../config';
import { useFocusEffect } from '@react-navigation/native';
import React, { useState, useEffect , useRef } from "react";
import { Ionicons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import Header from '../components/Header';
import { FontAwesome5 } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import storage from "@react-native-async-storage/async-storage";
import { useNavigation } from '@react-navigation/native';
const AppointmentsDoctor = () => {
  const imageUri = "https://cdn4.iconfinder.com/data/icons/medical-corona-healthcare/135/Doctor_Appointment-512.png"
  const [sortedCalendar , setSortedCalendar] = useState([]) ;
  const [calendarEmpty , setCalendarEmpty] = useState(true);
  const [calendar, setCalendar] = useState();
  useFocusEffect(
    React.useCallback(() => {
      firebase.firestore().collection('Doctors')
      .doc(firebase.auth().currentUser.uid).get()
      .then((snapshot) => {
        if (snapshot.exists) {
            const calendarData = [];
          const datas = snapshot.data();
          calendarData.push(datas.appointments);
          setCalendar(calendarData);
          console.log( Array.isArray(calendarData))
          if(calendarData.length > 0 && Array.isArray(calendarData) ) {
            const flattenedCalendar = calendarData.flat();
            console.log(flattenedCalendar)
              calendarData.forEach((appointmentArray) => {
                console.log(appointmentArray.length)
                if(Array.isArray(appointmentArray) && appointmentArray.length > 0){
                  setSortedCalendar(flattenedCalendar.sort(compareEvents));
                  setCalendarEmpty(false);
                  console.log(sortedCalendar)
                }
                else{
                  setCalendarEmpty(true);
                }
              }
              )
          }
        } else {
          console.log('User does not exist');
        }
      });
      console.log("Screen refreshed")
      return () => {
        // Cleanup code here
      };
    }, [])
  );
  useEffect(() => {
    firebase.firestore().collection('Doctors')
      .doc(firebase.auth().currentUser.uid).get()
      .then((snapshot) => {
        if (snapshot.exists) {
            const calendarData = [];
          const datas = snapshot.data();
          calendarData.push(datas.appointments);
          setCalendar(calendarData);
          console.log( Array.isArray(calendarData))
          if(calendarData.length > 0 && Array.isArray(calendarData) ) {
            const flattenedCalendar = calendarData.flat();
              calendarData.map((appointmentArray) => {
                if(Array.isArray(appointmentArray) && appointmentArray.length > 0){
                  setSortedCalendar(flattenedCalendar.sort(compareEvents));
                  setCalendarEmpty(false);
                  console.log(sortedCalendar)
                }
                else{
                  setCalendarEmpty(true);
                }
              }
              )
          }
        } else {
          console.log('User does not exist');
        }
      });
  }, []);

  const renderAddress = (address) => {
    if (typeof address === 'string') {
      return <Text style={{}}>{address}</Text>;
    } else if (typeof address === 'object' && address.hasOwnProperty('userAddress')) {
      return <Text style={{}}>{address.userAddress}</Text>;
    } else {
      return null; // Handle other cases if necessary
    }
  };

  const compareEvents = (a, b) => {
    const dateA = new Date(a.date);
    const dateB = new Date(b.date);

    if (dateA < dateB) return -1;
    if (dateA > dateB) return 1;

    const timeA = convertTimeTo24HourFormat(a.time);
    const timeB = convertTimeTo24HourFormat(b.time);

    if (timeA < timeB) return -1;
    if (timeA > timeB) return 1;

    return 0;
  };

  const convertTimeTo24HourFormat = (time) => {
    const [hour, minute, meridiem] = time.split(/:| /);
    
    let hour24 = parseInt(hour, 10);
    if (meridiem === "PM" && hour24 !== 12) {
      hour24 += 12;
    } else if (meridiem === "AM" && hour24 === 12) {
      hour24 = 0;
    }

    return hour24 * 100 + parseInt(minute, 10);
  };

  const isDatePassed = (dateString) => {
    const currentDate = new Date();
    const appointmentDate = new Date(dateString);

    return currentDate > appointmentDate;
  };
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const [userToken, setUserToken] = useState('');
  useFocusEffect(
    React.useCallback(() => {
    const getPermission = async () => {
          const { status: existingStatus } = await Notifications.getPermissionsAsync();
          let finalStatus = existingStatus;
          if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
          }
          if (finalStatus !== 'granted') {
            alert('Enable push notifications to use the app!');
            await storage.setItem('expopushtoken', "");
            return;
          }
          const token = (await Notifications.getExpoPushTokenAsync()).data;
          await storage.setItem('expopushtoken', token);
          setUserToken(token);
          console.log("Doctor:" +token);
          
          const doctorRef = firebase.firestore().collection('Doctors').doc(firebase.auth().currentUser.uid);
          doctorRef.update({
            token:token
          })
          .then(() => {
            console.log('Token update successful');
          })
          .catch((error) => {
            console.error('Error getting token:', error);
          });

       
        if (Platform.OS === 'android') {
          Notifications.setNotificationChannelAsync('default', {
            name: 'default',
            importance: Notifications.AndroidImportance.MAX,
            vibrationPattern: [0, 250, 250, 250],
            lightColor: '#FF231F7C',
          });
        }
    }

    getPermission();

    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {});

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, [])
  );
const navigation = useNavigation();
  
  return (
    <ScrollView style={{ flex: 1, backgroundColor: '#F0F0F0', marginTop: 5 }}>
      <Header collection="Doctors"/>
      {calendarEmpty ? (
        <SafeAreaView style={{flex:1 , flexDirection:'column' , alignItems:'center' , justifyContent:'center' , alignContent:'center', marginVertical:20}}>
            <Image
                source={{ uri: 'https://img.freepik.com/premium-vector/calendar-with-clock-icon-comic-style-agenda-cartoon-vector-illustration-white-isolated-background-schedule-time-planner-splash-effect-business-concept_157943-10639.jpg?w=2000' }}
                style={{
                width: 400,
                height: 400,
                borderRadius: 40,
                marginRight: 10,
                }}
            />
            <Text style={{color:"black" , fontSize:30 , marginTop:28}}>You have no appointment yet</Text>
        </SafeAreaView>
      ) : (
      <FlatList
        data={sortedCalendar}
        renderItem={({ item }) => {
          const isPassed = isDatePassed(item.date);
          return (
            <Pressable onPress={()=>navigation.navigate("UpdateAppointment" , {collection:"Doctors" , appointment:item})} style={styles.container}>
              <View style={[styles.cardContainer, isPassed && styles.cardContainerPassed]}>
                {isPassed && (
                  <View style={styles.outOfDateContainer}>
                    <Entypo name="circle-with-cross" size={24} color="red" />
                    <Text style={styles.outOfDateText}>Out of Date</Text>
                  </View>
                )}
                <Image style={styles.imageStyle} source={{ uri:  imageUri }} />
                <View style={styles.infoStyle}>
                  <Text style={styles.titleStyle}>{item.patientName}</Text>
                  <Text style={styles.categoryStyle}>{item.service}</Text>

                  <View style={styles.iconLabelStyle}>
                    <View style={{ alignItems: 'center', flex: 1, flexDirection: 'column', marginBottom: 8, marginHorizontal: 7 }}>
                      <Ionicons name="ios-timer" size={24} color="#007260" />
                      <Text style={{}}>{item.date} at {item.time}</Text>
                    </View>
                    <View style={{ alignItems: 'center', flex: 1, flexDirection: 'column', marginBottom: 8, marginRight: 7 }}>
                      <Entypo name="location-pin" size={24} color="#007260" />
                      {renderAddress(item.address)}
                    </View>
                  </View>
                </View>
              </View>
            </Pressable>
          );
        }}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.id}
      />)}
    </ScrollView>
  );
};

const deviceWidth = Math.round(Dimensions.get('window').width);
const offset = 10;
const radius = 20;

const styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    alignItems: 'center',
    marginTop: 25,
  },
  cardContainer: {
    width: deviceWidth - offset,
    backgroundColor: '#F0F0F0',
    height: 245,
    borderRadius: radius,
    shadowColor: '#000',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.75,
    shadowRadius: 5,
    elevation: 9,
  },
  cardContainerPassed: {
    borderWidth: 1,
    borderColor: 'red',
  },
  outOfDateContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 10,
    zIndex:20
  },
  outOfDateText: {
    fontSize: 12,
    color: 'red',
    marginLeft: 5,
  },
  imageStyle: {
    height: 130,
    width: deviceWidth - offset,
    borderTopLeftRadius: radius,
    borderTopRightRadius: radius,
    opacity: 0.9,
    alignContent: 'center',
    alignSelf: 'center',
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: '800',
  },
  categoryStyle: {
    fontWeight: '300',
    color: '#007260',
    marginTop: 5,
  },
  infoStyle: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  iconLabelStyle: {
    flexDirection: 'row',
  },
});

export default AppointmentsDoctor;
