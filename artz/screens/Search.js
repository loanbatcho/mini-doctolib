import { Searchbar } from 'react-native-paper';
import { useState } from 'react';
import { View, Text, StatusBar, Image, Pressable, TextInput, TouchableOpacity  , SafeAreaView} from 'react-native'

function Search(){
    const [input, setInput] = useState("");
    return (
		<View style={{position: 'sticky', top: 0 , zIndex:999}}>
			<Searchbar
				style={{marginTop:12,}}
				placeholder="Search"
				onChangeText={(text) => {
					setInput(text);}}
				value={input}
			/>
			<StatusBar style="auto" />
		</View>
	);
}

export default Search