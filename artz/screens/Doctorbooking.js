import {firebase} from '../config'
import React, { useState, useEffect, useRef } from "react";
import * as Calendar from 'expo-calendar' 
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import storage from "@react-native-async-storage/async-storage";
import { StyleSheet, Alert , Platform, Image,  Text, View,  SafeAreaView, ScrollView, Pressable, TextInput  } from "react-native";
import HorizontalDatepicker from "@awrminkhodaei/react-native-horizontal-datepicker";
import { useNavigation, useRoute  } from "@react-navigation/native";
import axios from "axios";
import RNPickerSelect from 'react-native-picker-select';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true
  })
});
const DoctorBooking = (props) => {
  
    const patientId = firebase.auth().currentUser.uid;
    const [selectedDate, setSelectedDate] = useState((String) (new Date()));
    const [selectedTime, setSelectedTime] = useState("");
    const [doctorCalendar, setDoctorCalendar] = useState([]);
  
    const navigation = useNavigation();
    const route = useRoute();
    const doctorId = route.params.doctor.id;
    const doctor = route.params.doctor;
    const patient = route.params.patient;
    const userAddress = route.params.uAddress;
    // Fetch the doctor's calendar from the Firestore collection
    const [selectedAddress, setSelectedAddress] = useState('');
    const [selectedService, setSelectedService] = useState('');
    const [userInfo, setUserinfo] = useState('');
    useEffect( ()=> {
        firebase.firestore().collection('patients')
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());  
                          }
            else{
                console.log('User does not exist');
            }
        })
      } , [])
    useEffect(() => {
      firebase.firestore()
    .collection("Doctors")
    .doc(doctorId)
    .get()
    .then((snapshot) => {
      if (snapshot.empty) {
        console.log("Error getting doctor's calendar");
      } else {
          const calendarData = [];
          const datas = snapshot.data();
          calendarData.push(datas.appointments);
          setDoctorCalendar(calendarData);
      }
    })
    .catch((error) => {
      console.error("Error retrieving doctor's calendar:", error);
    });
  
    }, []);
    const [times , setTimes] = useState([]);
    useEffect(()=>{
      
      const timeTemp = [];
      for (let i = 8; i <= 21; i++) {
          const hour = i % 12 || 12;
          const meridiem = i < 12 ? "AM" : "PM";
        
          const time = `${hour.toString().padStart(2, "0")}:00 ${meridiem}`;
        
          timeTemp.push({
            id: i.toString(),
            time: time,
          });
        }
        setTimes(timeTemp);
  
        var dateObject = String(selectedDate);
        const date =  formatDate(dateObject.split(" ").slice(0, 4).join(" "));
        let filteredTimes = [...timeTemp];
        
        if (doctorCalendar.length > 0 && Array.isArray(doctorCalendar)) {
          const flattenedCalendar = doctorCalendar.flat();
          flattenedCalendar.map((appointment) => {
            if((appointment.date !== "") && (appointment.date !== undefined)){
              const appointmentDate = appointment.date;
              console.log(date);
              console.log(appointmentDate);
              if (appointmentDate=== date) {
                filteredTimes = filteredTimes.filter(
                  (value) => value.time !== appointment.time
                );
              }}
          });
        }
        setTimes(filteredTimes);
      }
      
  ,[selectedDate , selectedTime , doctorCalendar])

    const addAppointment = (date) => {
      const dateToregister = formatDate(selectedDate);
      const selectedDoctorName = doctor.firstname +" " +doctor.lastname;
      const selectedPatient = patient.firstname + " " + patient.lastname;
      const doctorRef = firebase.firestore().collection('Doctors').doc(doctorId);
      doctorRef.update({
        appointments: firebase.firestore.FieldValue.arrayUnion({
          service: selectedService,
          date: dateToregister,
          time: selectedTime,
          patientName: selectedPatient,
          address: selectedAddress,
          doctorId:doctor.id,
          patientId:patientId
        })
      })
        .then(() => {
          console.log('Appointment added successfully');
          // sendEmail();
          sendNotification(doctor , patient);
          navigation.navigate("Confirmbooking");
        })
        .catch((error) => {
          console.error('Error adding appointment:', error);
        });

        const patientRef = firebase.firestore().collection('patients').doc(patientId);
        patientRef.update({
          appointments: firebase.firestore.FieldValue.arrayUnion({
            service: selectedService,
            date: dateToregister,
            time: selectedTime,
            doctorName: selectedDoctorName,
            address: selectedAddress,
            doctorId:doctorId,
            patientId:patientId
          })
        })
        .then(() => {
          console.log('Appointment added successfully');
        })
        .catch((error) => {
          console.error('Error adding appointment:', error);
        });
    };
    
    
    const bookDoctor = () => {
      const date = String(selectedDate).split(' ').slice(0, 4).join(' ');
      if (selectedTime !== '' && selectedAddress !== "" && selectedService !=='') {
        const fullHoraire = formatDate_and_time(selectedDate, selectedTime);
        Alert.alert(
          'Confirm booking',
          'Please confirm your appointment with ' +
            doctor.firstname +
            ' ' +
            doctor.lastname +
            ' for ' +
            fullHoraire,
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                addAppointment(date);      
              },
            },
          ],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          'Missing information',
          'Please select all the required informations.',
          [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
          { cancelable: false }
        );
      }
    };
    
    function formatDate(dateSend){
      const date = new Date(dateSend);
  
      const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  
      const day = weekdays[date.getDay()];
      const dayOfMonth = date.getDate();
      const month = months[date.getMonth()];
      const year = date.getFullYear();
  
      const formattedDate = `${day}, ${dayOfMonth} ${month} ${year}`;
      console.log(formattedDate); // Output: Sunday, 25 June 2023
  
      const fullHoraire = formattedDate;
      return fullHoraire;
    }

  
    function formatDate_and_time(dateSend , timeSend){
      const date = new Date(dateSend);
  
      const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  
      const day = weekdays[date.getDay()];
      const dayOfMonth = date.getDate();
      const month = months[date.getMonth()];
      const year = date.getFullYear();
  
      const formattedDate = `${day}, ${dayOfMonth} ${month} ${year}`;
      console.log(formattedDate); // Output: Sunday, 25 June 2023
  
      const fullHoraire = formattedDate + " at " +timeSend;
      return fullHoraire;
    }
  
    // const sendEmail = async () => {
    //   try {
    //     const fullHoraire = formatDate_and_time(selectedDate  , selectedTime);
    //     console.log(fullHoraire);
    //     const formData = new FormData();
    //     formData.append('email', patient.email);
    //     formData.append('doctorName', doctor.firstname +" " + doctor.lastname);
    //     formData.append('date', fullHoraire);
    //     formData.append('patientName', patient.firstname +" " +patient.lastname);
    //     formData.append('doctorEmail', doctor.email);
    //     const response = await axios.post('http://localhost/artz_apis/api_send_confirmation_booking.php', formData);
    //     // console.log(response.data);
    //   } catch (error) {
    //     console.error(error);
    //   }
    // };
    
  
    // const addEventToCalendar = async () => {
    //   try {
    //   const { status } = await Calendar.requestCalendarPermissionsAsync()
    //   if (status === 'granted') {
    //     console.log("Here 1");
    //   console.log('Permissions granted. Fetching available calendars...')
    //   console.log("Here 2");
    //   const startDate = new Date(); // Current date and time
    //   const endDate = new Date();
    //   endDate.setHours(endDate.getHours() + 1); // Adding 1 hour to the start time
      
    //   const notes = "This is a sample event note.";
    //   const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone; // Using the device's current time zone
      
    //   const eventConfig = {
    //     title: 'Event Title',
    //     startDate: startDate,
    //     endDate: endDate,
    //     location: 'Event Location',
    //     notes: notes,
    //     timeZone: timeZone,
    //   };
    //   console.log('eventConfig:', eventConfig);
    //   const eventId = await Calendar.createEventAsync(3, eventConfig)
    //   console.log(eventId)
    //   Alert.alert('Success', 'Event added to your calendar')
    //   console.log("Good");
    //   } else {
    //   console.warn('Calendar permission not granted.')
    //   console.log("Here 4 BAD");
    //   }
    //   } catch (error) {
    //   console.warn(error)
    //   }
    //   } 

    const [notification, setNotification] = useState(false);
    const notificationListener = useRef();
    const responseListener = useRef();
    const [userToken, setUserToken] = useState('');
    useEffect(() => {
      const getPermission = async () => {
            const { status: existingStatus } = await Notifications.getPermissionsAsync();
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
              const { status } = await Notifications.requestPermissionsAsync();
              finalStatus = status;
            }
            if (finalStatus !== 'granted') {
              alert('Enable push notifications to use the app!');
              await storage.setItem('expopushtoken', "");
              return;
            }
            const token = (await Notifications.getExpoPushTokenAsync()).data;
            await storage.setItem('expopushtoken', token);
            setUserToken(token);
            console.log("Patient:" +token);
            const patientRef = firebase.firestore().collection('patients').doc(firebase.auth().currentUser.uid);
            patientRef.update({
              token:token
            })
            .then(() => {
              console.log('Token update successful');
            })
            .catch((error) => {
              console.error('Error getting token:', error);
            });
  
          if (Platform.OS === 'android') {
            Notifications.setNotificationChannelAsync('default', {
              name: 'default',
              importance: Notifications.AndroidImportance.MAX,
              vibrationPattern: [0, 250, 250, 250],
              lightColor: '#FF231F7C',
            });
          }
      }
  
      getPermission();
  
      notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
        setNotification(notification);
      });
  
      responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {});
  
      return () => {
        Notifications.removeNotificationSubscription(notificationListener.current);
        Notifications.removeNotificationSubscription(responseListener.current);
      };
    }, []);
    
  

  const sendNotification = async (doctoR , patient) => {
    const scheduledDate = new Date(selectedDate); // Convert selectedDate to a Date object
    const [time, meridiem] = selectedTime.split(' ');
    const [hours, minutes] = time.split(':');

    // Convert hours to 24-hour format if necessary
    let adjustedHours = parseInt(hours, 10);
    if (meridiem === 'PM' && adjustedHours !== 12) {
      adjustedHours += 12;
    } else if (meridiem === 'AM' && adjustedHours === 12) {
      adjustedHours = 0;
    }

    scheduledDate.setHours(adjustedHours);
    scheduledDate.setMinutes(parseInt(minutes-10)); // Set the date of remind 10 min before the appointment
    console.log(scheduledDate);
    const currentDate = new Date();
    const fullHoraire = formatDate_and_time(selectedDate, selectedTime);

  // Set the trigger to 1 minute after the current time
    const trigger = {
      seconds: currentDate.getSeconds() + 2, // Add 60 seconds to current seconds
      repeats: false // Set repeats to false to trigger only once
    };
    console.log(trigger);

    const triggerRemind = {
      date:scheduledDate,
      repeats: false // Set repeats to false to trigger only once
    };

    await Notifications.scheduleNotificationAsync({
      content: {
        title: "GOOD NEWS 🤗",
        body: "Your appointment is confirmed for: " + fullHoraire + " 🎇 \n" 
        +"Prepare to meet : " +doctor.gender +" " +doctor.firstname +" " +doctor.lastname +" 👨🏽‍⚕️ \n"
        +"You'll be remind 10 minutes before the actual appointment datee.",
        data: { data: "data goes here" }
      },
      trigger: trigger
    });

    await Notifications.scheduleNotificationAsync({
      content: {
        title: "APPOINTMENT IN 10 MINUTES 😷",
        body: "Don't forget your appointment at: " + fullHoraire + " 🎇 \n" 
        +doctor.gender +" " +doctor.firstname +" " +doctor.lastname +" is waiting for you 👨🏽‍⚕️ \n"
        +"Hurry up , it is in 10 minutes 😲",
        data: { data: "data goes here" }
      },
      trigger: triggerRemind
    });


    const doctorNotificationContent = {
      title: 'New Appointment',
      body: 'You have a new appointment 📅 the ' + fullHoraire +"\n Your future patient is :  " +patient.lastname +" " +patient.firstname 
      +"\n This appointment will be remind to you 10 minutes before the moment 😉.",
    };
    
    const doctorNotificationRequest = {
      content: doctorNotificationContent,
      to: doctoR.token,
      trigger: null,
    };
    


    const doctorNotificationRemindContent = {
      title: 'Reminder 😷',
      body: "Don't forget your appointment at: " + fullHoraire + " 🎇 \n" 
      +patient.gender +" " +patient.firstname +" " +patient.lastname +" is waiting for you 👨🏽 \n"
      +"Hurry up , it is in 10 minutes 😲"
    };
    
    const doctorNotificationRequestREMIND = {
      content: doctorNotificationRemindContent,
      to: doctoR.token,
      trigger: triggerRemind,
    };

    try {
      const doctorNotificationResponse = await Notifications.scheduleNotificationAsync(
        doctorNotificationRequestREMIND
      );
      console.log('Notification sent to the doctor:', doctorNotificationResponse);
    } catch (error) {
      console.error('Error sending notification to the doctor:', error);
    }

    try {
      const doctorNotificationResponse = await Notifications.scheduleNotificationAsync(
        doctorNotificationRequest
      );
      console.log('Notification sent to the doctor:', doctorNotificationResponse);
    } catch (error) {
      console.error('Error sending notification to the doctor:', error);
    }
    
  }


  return (
    <ScrollView style={styles.headerContainer}>
      <Image
        source={{uri: doctor.image }}
        style={styles.coverImage}
      />
      <View style={styles.avatarContainer}>
        <Image
          source={{ uri: doctor.image}}
          style={styles.avatar}
        />
        <Text style={[styles.name, styles.textWithShadow]}>{doctor.firstname} {doctor.lastname}</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Email:</Text>
          <Text style={styles.infoValue}>{doctor.email}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Location:</Text>
          <Text style={styles.infoValue}>{doctor.location}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Bio:</Text>
          <Text style={styles.infoValue}>{doctor.bio}</Text>
        </View>
      </View>
      <Text style={{ fontSize: 16, fontWeight: "500", marginHorizontal: 10 }}>Select Address </Text>
        <RNPickerSelect
          value={selectedAddress}
          onValueChange={(itemValue) => setSelectedAddress(itemValue)}
          items={[
            { label: "Doctor's place", value: doctor.location },
            { label: "Your place ", value: patient.location },
            { label: "Current location", value: userAddress },
          ]}
          style={pickerSelectStyles} // Apply the custom styles
          placeholder={{
            label: "Select an address",
            value: "",
          }}
        />
        <Text style={{ fontSize: 16, fontWeight: "500", marginHorizontal: 10 }}>Select Service </Text>
        <RNPickerSelect
          value={selectedService}
          onValueChange={(itemValue) => setSelectedService(itemValue)}
          items={doctor.speciality.map((speciality) => ({
            label: speciality,
            value: speciality,
          }))}
          style={pickerSelectStyles} // Apply the custom styles
          placeholder={{
            label: "Select a service",
            value: "",
          }}
        />
          
      <Text style={{ fontSize: 16, fontWeight: "500", marginHorizontal: 10 }}>Select Date</Text>
      <HorizontalDatepicker
          mode="gregorian"
          startDate={new Date()} // Set the start date to the current date
          endDate={new Date("2023-12-31")} // Set the end date to December 31, 2023
          initialSelectedDate={new Date()} // Set the initial selected date to the current date
          onSelectedDateChange={(date) => setSelectedDate(date)}
          selectedItemWidth={170}
          unselectedItemWidth={38}
          itemHeight={38}
          itemRadius={10}
          selectedItemTextStyle={styles.selectedItemTextStyle}
          unselectedItemTextStyle={styles.selectedItemTextStyle}
          selectedItemBackgroundColor="#007260"
          unselectedItemBackgroundColor="#ececec"
          flatListContainerStyle={styles.flatListContainerStyle}
        />

        <Text style={{ fontSize: 16, fontWeight: "500", marginHorizontal: 10 }}>Select Time</Text>

        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {times.map((item) => {
            const isDisabled = doctorCalendar.includes(item.time); // Check if the time slot is in the doctor's calendar

            return (
              <View
                key={item.id}
                style={[
                  styles.timeButton,
                  selectedTime === item.time && styles.selectedTimeButton,
                  isDisabled && styles.disabledTimeButton,
                ]}
              >
                <Text
                  onPress={() => !isDisabled && setSelectedTime(item.time)}
                  style={[
                    styles.timeButtonText,
                    selectedTime === item.time && styles.selectedTimeButtonText,
                    isDisabled && styles.disabledTimeButtonText,
                  ]}
                >
                  {item.time}
                </Text>
              </View>
            );
          })}
        </ScrollView>
        
        
        {/* Proceed button */}
        <Pressable onPress={bookDoctor} style={styles.proceedButton}>
          <Text style={styles.proceedButtonText}>Proceed to booking</Text>
        </Pressable>
    </ScrollView>
  );
};

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    marginBottom:7,
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 2,
    borderColor: 'gray', // Green border color
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // To ensure the text is not obscured by the dropdown arrow
  },
  inputAndroid: {
    marginBottom:7,
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 2,
    borderColor: 'gray', // Green border color
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // To ensure the text is not obscured by the dropdown arrow
  },
});
const styles = StyleSheet.create({
    addressInput: {
      borderWidth: 1,
      borderColor: '#ccc',
      borderRadius: 5,
      padding: 10,
      marginHorizontal: 10,
      marginVertical: 5,
    },
    headerContainer: {
        flexDirection: "column",
        paddingHorizontal: 10,
      },
      headerTitle: {
        fontSize: 16,
        fontWeight: "bold",
        marginLeft: 10,
      },
      selectedItemTextStyle: {
        color: "#fff",
        fontWeight: "bold",
      },
      flatListContainerStyle: {
        marginTop: 10,
      },
      timeButton: {
        margin: 10,
        borderRadius: 7,
        padding: 15,
        borderColor: "gray",
        borderWidth: 0.7,
      },
      selectedTimeButton: {
        borderColor: "#007260",
      },
      disabledTimeButton: {
        backgroundColor: "red",
      },
      timeButtonText: {
        fontSize: 16,
      },
      selectedTimeButtonText: {
        color: "#007260",
      },
      disabledTimeButtonText: {
        color: "#fff",
      },
      proceedButton: {
        marginTop: 20,
        marginHorizontal: 10,
        backgroundColor: "#007260",
        padding: 15,
        borderRadius: 10,
        alignItems: "center",
      },
      proceedButtonText: {
        color: "#fff",
        fontSize: 16,
        fontWeight: "bold",
      },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  coverImage: {
    height: 200,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  avatar: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 10,
    color:'white'
  },
  content: {
    marginTop: 20,
    marginBottom: 20,
  },
  infoContainer: {
    marginTop: 20,
  },
  infoLabel: {
    fontWeight: 'bold',
  },
  infoValue: {
    marginTop: 5,
  },
});

export default DoctorBooking