
import {firebase} from '../config'
import React, { useState, useEffect } from "react";
import * as Calendar from 'expo-calendar' 
import * as Permissions from 'expo-permissions';
import { StyleSheet, Alert , Platform, Image,  Text, View, SafeAreaView, ScrollView, Pressable, TouchableOpacity } from "react-native";
import HorizontalDatepicker from "@awrminkhodaei/react-native-horizontal-datepicker";
import { useNavigation, useRoute } from "@react-navigation/native";
import { Route } from "@react-navigation/native";
import axios from "axios";
import Button from "../components/Button";
const ProfileScreen = ({ navigation }) => {

    const patientId = firebase.auth().currentUser.uid;
    const [selectedDate, setSelectedDate] = useState((String) (new Date()));
    const [selectedTime, setSelectedTime] = useState("");
    const [doctorCalendar, setDoctorCalendar] = useState([]);
  
    const navigation = useNavigation();
    const route = useRoute();
    const doctorId = route.params.doctor.id;
    const doctor = route.params.doctor;
    const patient = route.params.patient;
    // Fetch the doctor's calendar from the Firestore collection
    useEffect(() => {
      firebase.firestore()
    .collection("Doctors")
    .doc(doctorId)
    .get()
    .then((snapshot) => {
      if (snapshot.empty) {
        console.log("Error getting doctor's calendar");
      } else {
          const calendarData = [];
          const datas = snapshot.data();
          calendarData.push(datas.appointments);
          setDoctorCalendar(calendarData);
      }
    })
    .catch((error) => {
      console.error("Error retrieving doctor's calendar:", error);
    });
  
    }, []);
  
    useEffect(()=>{
      const timeTemp = [];
      for (let i = 8; i <= 21; i++) {
          const hour = i % 12 || 12;
          const meridiem = i < 12 ? "AM" : "PM";
        
          const time = `${hour.toString().padStart(2, "0")}:00 ${meridiem}`;
        
          timeTemp.push({
            id: i.toString(),
            time: time,
          });
        }
        setTimes(timeTemp);
  
      var dateObject = (String)(selectedDate);
      const date = dateObject.split(" ").slice(0, 4).join(" ");;
      let filteredTimes = [...timeTemp];
      if (doctorCalendar.size > 0) {
        doctorCalendar.forEach((appointmentArray) => {
          appointmentArray.forEach((appointment) => {
            if (appointment.date === date) {
              filteredTimes = filteredTimes.filter(
                (value) => value.time !== appointment.time
              );
            }
          });
        });
      }     
        setTimes(filteredTimes);
      }
      
  ,[selectedDate , selectedTime , doctorCalendar])
  
    const [times , setTimes] = useState([]);
  
    const handleBackButton = (date) => {
      navigation.navigate("Homescreen");
    };
    
  
    const addAppointment = (date) => {
      const doctorRef = firebase.firestore().collection('Doctors').doc(doctorId);
    
      doctorRef.update({
        appointments: firebase.firestore.FieldValue.arrayUnion({ patientId ,date, time:selectedTime  })
      })
        .then(() => {
          console.log('Appointment added successfully');
          sendEmail();
          navigation.navigate("Confirmbooking");
        })
        .catch((error) => {
          console.error('Error adding appointment:', error);
        });
    };
    
    const bookDoctor = () => {
      const date = (String)(selectedDate).split(" ").slice(0, 4).join(" ");
      if(selectedTime !== ""){
          const fullHoraire = formatDate_and_time(selectedDate  , selectedTime);
          // Alert.alert(
              
          //     "Confirm booking",
          //     "Please confirm your appointment with " +doctor.firstname +" " +
          //     doctor.lastname +" for " +fullHoraire ,
          //     [
          //       {
          //         text: "Cancel",
          //         onPress: () => console.log("Cancel Pressed"),
          //         style: "cancel",
          //       },
          //       { text: "OK", onPress: () =>{addAppointment(date)} },
          //     ],
          //     { cancelable: false }
          //   );
          addAppointment(date);
      }
      else{
          Alert.alert(
              "Missing informations",
              "Please select date and time",
              [
                { text: "OK", onPress: () => console.log("OK Pressed") },
              ],
              { cancelable: false }
            );
      }
    };
  
    function formatDate_and_time(dateSend , time){
      const date = new Date(dateSend);
  
      const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  
      const day = weekdays[date.getDay()];
      const dayOfMonth = date.getDate();
      const month = months[date.getMonth()];
      const year = date.getFullYear();
  
      const formattedDate = `${day}, ${dayOfMonth} ${month} ${year}`;
      console.log(formattedDate); // Output: Sunday, 25 June 2023
  
      const fullHoraire = formattedDate + " at " +time;
      return fullHoraire;
    }
  
    const sendEmail = async () => {
      try {
        const fullHoraire = formatDate_and_time(selectedDate  , selectedTime);
        console.log(fullHoraire);
        const formData = new FormData();
        formData.append('email', patient.email);
        formData.append('doctorName', doctor.firstname +" " + doctor.lastname);
        formData.append('date', fullHoraire);
        formData.append('patientName', patient.firstname +" " +patient.lastname);
        formData.append('doctorEmail', doctor.email);
        const response = await axios.post('http://localhost/artz_apis/api_send_confirmation_booking.php', formData);
        // console.log(response.data);
      } catch (error) {
        console.error(error);
      }
    };
    
  
    const addEventToCalendar = async () => {
      try {
      const { status } = await Calendar.requestCalendarPermissionsAsync()
      if (status === 'granted') {
        console.log("Here 1");
      console.log('Permissions granted. Fetching available calendars...')
      console.log("Here 2");
      const startDate = new Date(); // Current date and time
      const endDate = new Date();
      endDate.setHours(endDate.getHours() + 1); // Adding 1 hour to the start time
      
      const notes = "This is a sample event note.";
      const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone; // Using the device's current time zone
      
      const eventConfig = {
        title: 'Event Title',
        startDate: startDate,
        endDate: endDate,
        location: 'Event Location',
        notes: notes,
        timeZone: timeZone,
      };
      console.log('eventConfig:', eventConfig);
      const eventId = await Calendar.createEventAsync(3, eventConfig)
      console.log(eventId)
      Alert.alert('Success', 'Event added to your calendar')
      console.log("Good");
      } else {
      console.warn('Calendar permission not granted.')
      console.log("Here 4 BAD");
      }
      } catch (error) {
      console.warn(error)
      }
      } 
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: 'https://www.solidbackgrounds.com/images/1920x1080/1920x1080-green-web-color-solid-color-background.jpg' }}
        style={styles.coverImage}
      />
      <View style={styles.avatarContainer}>
        <Image
          source={{ uri: 'https://www.bootdey.com/img/Content/avatar/avatar1.png' }}
          style={styles.avatar}
        />
        <Text style={[styles.name, styles.textWithShadow]}>{userInfo.firstname} {userInfo.lastname}</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Email:</Text>
          <Text style={styles.infoValue}>{userInfo.email}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Location:</Text>
          <Text style={styles.infoValue}>{userInfo.location}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Bio:</Text>
          <Text style={styles.infoValue}>{userInfo.bio}</Text>
        </View>
      </View>
      <HorizontalDatepicker
          mode="gregorian"
          startDate={new Date()} // Set the start date to the current date
          endDate={new Date("2023-12-31")} // Set the end date to December 31, 2023
          initialSelectedDate={new Date()} // Set the initial selected date to the current date
          onSelectedDateChange={(date) => setSelectedDate(date)}
          selectedItemWidth={170}
          unselectedItemWidth={38}
          itemHeight={38}
          itemRadius={10}
          selectedItemTextStyle={styles.selectedItemTextStyle}
          unselectedItemTextStyle={styles.selectedItemTextStyle}
          selectedItemBackgroundColor="#007260"
          unselectedItemBackgroundColor="#ececec"
          flatListContainerStyle={styles.flatListContainerStyle}
        />

        <Text style={{ fontSize: 16, fontWeight: "500", marginHorizontal: 10 }}>Select Time</Text>

        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {times.map((item) => {
            const isDisabled = doctorCalendar.includes(item.time); // Check if the time slot is in the doctor's calendar

            return (
              <View
                key={item.id}
                style={[
                  styles.timeButton,
                  selectedTime === item.time && styles.selectedTimeButton,
                  isDisabled && styles.disabledTimeButton,
                ]}
              >
                <Text
                  onPress={() => !isDisabled && setSelectedTime(item.time)}
                  style={[
                    styles.timeButtonText,
                    selectedTime === item.time && styles.selectedTimeButtonText,
                    isDisabled && styles.disabledTimeButtonText,
                  ]}
                >
                  {item.time}
                </Text>
              </View>
            );
          })}
        </ScrollView>
        <Button
            title=" Add to calendar"
            filled
            style={{
                marginTop: 18,
                marginBottom: 4,
                padding : 10
            }}
            onPress={addEventToCalendar}                   
      />
        {/* Proceed button */}
        <Pressable onPress={bookDoctor} style={styles.proceedButton}>
          <Text style={styles.proceedButtonText}>Proceed to booking</Text>
        </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: "column",
        paddingHorizontal: 10,
      },
      headerTitle: {
        fontSize: 16,
        fontWeight: "bold",
        marginLeft: 10,
      },
      selectedItemTextStyle: {
        color: "#fff",
        fontWeight: "bold",
      },
      flatListContainerStyle: {
        marginTop: 10,
      },
      timeButton: {
        margin: 10,
        borderRadius: 7,
        padding: 15,
        borderColor: "gray",
        borderWidth: 0.7,
      },
      selectedTimeButton: {
        borderColor: "#007260",
      },
      disabledTimeButton: {
        backgroundColor: "red",
      },
      timeButtonText: {
        fontSize: 16,
      },
      selectedTimeButtonText: {
        color: "#007260",
      },
      disabledTimeButtonText: {
        color: "#fff",
      },
      proceedButton: {
        marginTop: 20,
        marginHorizontal: 10,
        backgroundColor: "#007260",
        padding: 15,
        borderRadius: 10,
        alignItems: "center",
      },
      proceedButtonText: {
        color: "#fff",
        fontSize: 16,
        fontWeight: "bold",
      },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  coverImage: {
    height: 200,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  avatar: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 10,
    color:'white'
  },
  content: {
    marginTop: 20,
  },
  infoContainer: {
    marginTop: 20,
  },
  infoLabel: {
    fontWeight: 'bold',
  },
  infoValue: {
    marginTop: 5,
  },
});

export default ProfileScreen;