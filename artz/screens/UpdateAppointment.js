import React from 'react';
import { View, Text, Image, StyleSheet , ScrollView , Alert } from 'react-native';
import {firebase} from '../config'
import { useEffect, useState } from 'react';
import Button from '../components/Button';
import { useNavigation, useRoute } from '@react-navigation/native';

const UpdateAppointment = () => {
  const navigation = useNavigation();
    const route = useRoute();
    const collection = (String) (route.params.collection);
    const appointment = (route.params.appointment);
    console.log(appointment)
    const [userInfo, setUserinfo] = useState('');
    const [waiter, setWaiter] = useState('');
    const [secondCollection, setSecondCollection] = useState('');
    useEffect( ()=> {
        firebase.firestore().collection(collection)
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());            }
            else{
                console.log('User does not exist');
            }
        })
        if(collection==="Doctors"){
            setSecondCollection("patients");
            firebase.firestore().collection("patients")
            .doc(appointment.patientId).get()
            .then( (snapschot) =>{
                if(snapschot.exists){
                    console.log(snapschot.data());
                    setWaiter(snapschot.data());            
                }
                else{
                    console.log('User does not exist');
                }
            })
        }
        else if(collection==="patients"){
            setSecondCollection("Doctors");
            firebase.firestore().collection("Doctors")
            .doc(appointment.doctorId).get()
            .then( (snapschot) =>{
                if(snapschot.exists){
                    console.log(snapschot.data())
                    setWaiter(snapschot.data());         }
                else{
                    console.log('User does not exist');
                }
            })
        }
        
      } , [])
      
      const renderAddress = (address) => {
        if (typeof address === 'string') {
          return <Text style={{}}>{address}</Text>;
        } else if (typeof address === 'object' && address.hasOwnProperty('userAddress')) {
          return <Text style={{}}>{address.userAddress}</Text>;
        } else {
          return null; // Handle other cases if necessary
        }
      };
      const cancelAppointment = () => {
        const currentUserID = firebase.auth().currentUser.uid;
        const appointmentsCollection = firebase.firestore().collection(collection);
      
        appointmentsCollection
          .doc(currentUserID)
          .get()
          .then((doc) => {
            if (doc.exists) {
              const appointments = doc.data().appointments || [];
              const filteredAppointments = appointments.filter(
                (appt) => appt.date !== appointment.date || appt.time !== appointment.time
              );
      
              appointmentsCollection
                .doc(currentUserID)
                .update({
                  appointments: filteredAppointments,
                })
                .then(() => {
                  console.log("Appointment cancelled successfully");
                  // Perform any additional actions after cancellation
                })
                .catch((error) => {
                  console.log("Error cancelling appointment:", error);
                });
            } else {
              console.log("User document does not exist");
            }
          })
          .catch((error) => {
            console.log("Error fetching user document:", error);
          });
          cancelAppointmentWaiter();

      };
      
      const cancelAppointmentWaiter = () => {
        var currentUserID;
        if(secondCollection==="patients"){
             currentUserID = appointment.patientId;
        }
        else if(secondCollection==="Doctors"){
             currentUserID = appointment.doctorId;
        }
        
        const appointmentsCollection = firebase.firestore().collection(secondCollection);
      
        appointmentsCollection
          .doc(currentUserID)
          .get()
          .then((doc) => {
            if (doc.exists) {
              const appointments = doc.data().appointments || [];
              const filteredAppointments = appointments.filter(
                (appt) => appt.date !== appointment.date || appt.time !== appointment.time
              );
      
              appointmentsCollection
                .doc(currentUserID)
                .update({
                  appointments: filteredAppointments,
                })
                .then(() => {
                  console.log("Appointment cancelled successfully");
                  // Perform any additional actions after cancellation
                  Alert.alert(
                    'Information',
                    'Appointment delete successful ',
                    [
                      {
                        text: 'Great',
                        onPress: () => {
                          navigation.navigate("Appointments")
                        },
                      },
                    ],
                    { cancelable: false }
                  );
                })
                .catch((error) => {
                  console.log("Error cancelling appointment:", error);
                });
            } else {
              console.log("User document does not exist");
            }
          })
          .catch((error) => {
            console.log("Error fetching user document:", error);
          });
      };
      
  return (
    <ScrollView style={styles.container}>
      <Image
        source={{ uri: waiter.image }}
        style={styles.coverImage}
      />
      <View style={styles.avatarContainer}>
        <Image
          source={{ uri: waiter.image }}
          style={styles.avatar}
        />
        <Text style={[styles.name, styles.textWithShadow]}>{waiter.firstname} {waiter.lastname}</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Email:</Text>
          <Text style={styles.infoValue}>{waiter.email}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Location:</Text>
          <Text style={styles.infoValue}>{renderAddress(appointment.address)}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Date:</Text>
          <Text style={styles.infoValue}>{appointment.date} at {appointment.time}</Text>
        </View>
      </View>
      <Button
            title="Cancel appointment"
            filled
            style={{
                marginTop: 18,
                marginBottom: 4,
            }}
            onPress={cancelAppointment}               
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  coverImage: {
    height: 200,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  avatar: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  name: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 30,
    color:'black'
  },
  content: {
    marginTop: 20,
  },
  infoContainer: {
    marginTop: 20,
  },
  infoLabel: {
    fontWeight: 'bold',
  },
  infoValue: {
    marginTop: 5,
  },
});

export default UpdateAppointment;