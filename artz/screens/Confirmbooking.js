import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import React from "react";
import LottieView from "lottie-react-native";
import Button from "../components/Button";
const Confirmbooking = ({navigation}) => {
  return (
    <SafeAreaView>
      <LottieView
        source={require("../assets/thumbs.json")}
        style={{
          height: 360,
          width:300,
          alignSelf: "center",
          marginTop: 40,
          justifyContent: "center",
        }}
        autoPlay
        loop={false}
        speed={0.7}
      />

      <Text
        style={{
          marginTop: 40,
          fontSize: 19,
          fontWeight: "600",
          textAlign: "center",
        }}
      >
        Your booking is now complete ! 
      </Text>
    
    

      <LottieView
        source={require("../assets/sparkle.json")}
        style={{
          height: 300,
          position: "absolute",
          top: 100,
          width: 300,
          alignSelf: "center",
        }}
        autoPlay
        loop={false}
        speed={0.7}
      />
      <Button
            title="Great !"
            filled
            style={{
                marginTop: 18,
                marginBottom: 4,
                padding : 10
            }}
            onPress={() => {navigation.navigate("Homescreen")}}                   
      />
    </SafeAreaView>
  );
};

export default Confirmbooking;

const styles = StyleSheet.create({});