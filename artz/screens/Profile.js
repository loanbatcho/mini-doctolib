import { View, Text, Image, Pressable, TextInput, TouchableOpacity  , SafeAreaView} from 'react-native'
import { useEffect, useState } from 'react';
import COLORS from '../constants/colors';
import { Ionicons } from "@expo/vector-icons";
import Checkbox from "expo-checkbox"
import Button from '../components/Button';
import {firebase} from '../config'

function Profile(){

    const [userInfo, setUserinfo] = useState('');
    useEffect( ()=> {
        firebase.firestore().collection('patients')
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());            }
            else{
                console.log('User does not exist');
            }
        })
      } , [])
    return(
        <SafeAreaView style={{flex: 1 , justifyContent: "center" , alignItems : 'center' , backgroundColor: COLORS.white }}>
            <Text style={{
                        fontWeight: 800,
                    }}>
                {userInfo.firstname} {userInfo.lastname} this is your profile page 
            </Text>
        </SafeAreaView>
        
    )
}
export default Profile