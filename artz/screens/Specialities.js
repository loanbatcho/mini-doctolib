import React, { useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  FlatList,
} from 'react-native'

function Specialities(props){
  const optionlist = [
    {
      id: 1,
      title: 'You',
      color: '#FF4500',
      image: 'https://img.icons8.com/color/70/000000/name.png',
    },
    {
      id: 2,
      title: 'General',
      color: '#87CEEB',
      refe : props.refGeneral,
      image: 'https://static.vecteezy.com/system/resources/previews/008/383/021/original/red-anatomical-heart-with-a-cardiogram-heart-icon-for-medical-facilities-abstract-background-that-represents-a-healthy-heart-life-heart-medicine-vector.jpg',
    },
    {
      id: 3,
      title: 'Surgery',
      color: '#4682B4',
      refe : props.refSurgery,
      image: 'https://as2.ftcdn.net/v2/jpg/02/36/89/47/1000_F_236894795_l6vxVZ83UckojN3W9Nhnda03kVXyVyk6.jpg',
    },
    {
      id: 4,
      title: 'Family',
      color: '#6A5ACD',
      refe : props.refFamily,
      image: 'https://img.icons8.com/color/70/000000/family.png',
    },
    {
      id: 5,
      title: 'Psychiatry',
      color: '#FF69B4',
      refe : props.refPsy,
      image: 'https://img.myloview.com/stickers/neurology-rgb-color-icon-psychiatric-treatment-brain-health-diagnostic-disorder-and-disease-medicine-clinical-therapy-professional-specialized-healthcare-isolated-vector-illustration-700-248861257.jpg',
    },
    {
      id: 6,
      title: 'Pediatric',
      color: '#00BFFF',
      refe : props.refPediatry,
      image: 'https://media.istockphoto.com/id/1130575791/vector/doctor-with-overweight-child-icon.jpg?s=612x612&w=0&k=20&c=4Py2sp6xQzzPsPaMIM6cAHsn0QaqYg9lVSGVAJtgeMk=',
    },
    {
      id: 7,
      title: 'Optics',
      color: '#00FFFF',
      refe : props.refOptics,
      image: 'https://thumbs.dreamstime.com/b/hand-eye-optical-clinic-flat-color-line-icon-vector-symbol-sign-illustration-design-isolated-white-background-163177394.jpg',
    },
    {
      id: 8,
      title: 'Neurologie',
      color: '#20B2AA',
      refe : props.refNeurologie,
      image: 'https://img.freepik.com/vecteurs-premium/symbole-plat-neurologie-icones-recherche-cerveau_8071-16147.jpg',
    },
    {
      id: 9,
      title: 'Urologie',
      color: '#191970',
      refe : props.refUrologie,
      image: 'https://c8.alamy.com/compfr/2f771bc/icone-de-ligne-de-couleur-urologie-pictogramme-pour-page-web-application-mobile-promo-element-de-conception-ui-ux-gui-contour-modifiable-2f771bc.jpg',
    },
    {
      id: 10,
      title: 'Emergency',
      color: '#008080',
      refe : props.refEmergency,
      image: 'https://thumbs.dreamstime.com/b/emergency-rgb-color-icon-ambulance-response-accident-department-urgent-medical-care-center-healthcare-vehicle-hospital-isolated-193855192.jpg',
    },
  ]
  console.log(props);
  const [options, setOptions] = useState(optionlist)

  const clickEventListener = item => {
    Alert.alert(item.title)
  }

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.list}
        contentContainerStyle={styles.listContainer}
        data={options}
        horizontal={false}
        numColumns={2}
        keyExtractor={item => {
          return item.id
        }}
        renderItem={({ item }) => {
          return (
            <View ref={item.refe} >
              <TouchableOpacity
                style={[styles.card, { backgroundColor: item.color }]}
                onPress={() => {
                  props.navigation.navigate("Doctors");
                }}>
                <Image style={styles.cardImage} source={{ uri: item.image }} />
              </TouchableOpacity>

              <View style={styles.cardHeader}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[styles.title, { color: item.color }]}>{item.title}</Text>
                </View>
              </View>
            </View>
          )
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
    backgroundColor: '#fff',
  },
  list: {
    paddingHorizontal: 5,
    backgroundColor: '#fff',
  },
  listContainer: {
    alignItems: 'center',
  },
  /******** card **************/
  card: {
    shadowColor: '#474747',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
    marginVertical: 20,
    marginHorizontal: 40,
    backgroundColor: '#e2e2e2',
    //flexBasis: '42%',
    width: 120,
    height: 120,
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage: {
    height: 50,
    width: 50,
    alignSelf: 'center',
  },
  title: {
    fontSize: 24,
    flex: 1,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
})
export default Specialities;