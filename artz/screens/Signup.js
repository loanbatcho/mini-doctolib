import { View, Text, Image,StyleSheet, Alert,KeyboardAvoidingView,Platform,Pressable, TextInput, TouchableOpacity , SafeAreaView, ScrollView} from 'react-native'
import { useEffect, useState  } from 'react';
import COLORS from '../constants/colors';
import { Ionicons } from "@expo/vector-icons";
import Checkbox from "expo-checkbox"
import Button from '../components/Button';
import {firebase} from '../config'
import RNPickerSelect from 'react-native-picker-select';

const Signup = ({ navigation }) => {
    const [selectedGender, setSelectedGender] = useState('');
    const [isPasswordShown, setIsPasswordShown] = useState(false);
    const [isChecked, setIsChecked] = useState(false);
    const [isCheckedPatient, setIsCheckedPatient] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstname, setfirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [image, setImage] = useState('');
    const services = [
        "Neurologie",
        "Pediatrics",
        "Surgery",
        "General",
        "Urologie",
        "Psychatry",
        "Family",
      ];
      
      const [selectedServices, setSelectedServices] = useState([]);

const handleServiceSelection = (value, service) => {
  if (value) {
    setSelectedServices(prevServices => [...prevServices, service]);
  } else {
    setSelectedServices(prevServices =>
      prevServices.filter(selectedService => selectedService !== service)
    );
  }
  console.log(selectedServices);
};

    
      var registerUser = async (email, password, firstname, lastname) => {
        try {
          if (!isCheckedPatient && !isChecked) {
            alert("Please select whether you are a doctor or a patient");
            return;
          }
          if (firstname.trim() === '' || lastname.trim() === '') {
            alert("Enter your firstname and lastname");
            return;
          }
          if (isChecked && selectedServices.length === 0) {
            alert("Please select at least one service");
            return;
          }
          if (selectedGender.trim()=== "") {
            alert("Please select a gender");
            return;
          }else{
            if(selectedGender==="Mr"){
                setImage("https://cdn-icons-png.flaticon.com/512/5556/5556468.png")
            }
            else if(selectedGender === "Mrs"){
                setImage("https://cdn.icon-icons.com/icons2/1736/PNG/512/4043251-avatar-female-girl-woman_113291.png")
            }
          }
          if (password.trim().length < 6) {
            alert("Please set a password of at least 6 characters");
            return;
          }
          await firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(() => {
              firebase.auth().currentUser.updateProfile({
                handleCodeInApp: true,
                url: 'https://artz-bacdd.firebaseapp.com'
              })
                .then(() => {
                    Alert.alert(
                        'INFORMATION',
                        'Your registration is fully completed ' 
                         ,
                        [
                          {
                            text: 'OK',
                            onPress: () => {   
                            },
                          },
                        ],
                        { cancelable: false }
                      );
                }).catch((error) => { alert(error.message) })
            })
            .then(() => {
              var typeOfUser;
              if (isCheckedPatient) {
                typeOfUser = "patients";
                firebase.firestore().collection(typeOfUser)
                  .doc(firebase.auth().currentUser.uid)
                  .set({
                    firstname,
                    lastname,
                    email,
                    password,
                    typeOfUser: "patients",
                    bio: "I'm a nice patient , looking for healthy 😷",
                    location: "Germany",
                    image: "https://cdn-icons-png.flaticon.com/512/5556/5556529.png",
                    gender:selectedGender,
                    image:image,
                    appointments:[],
                    token:"",
                  });
              } else if (isChecked) {
                typeOfUser = "Doctors";
                firebase.firestore().collection(typeOfUser)
                  .doc(firebase.auth().currentUser.uid)
                  .set({
                    firstname,
                    lastname,
                    email,
                    password,
                    typeOfUser: "Doctors",
                    bio: "I'm a cool doctor 😁",
                    location: "Germany",
                    image: "https://cdn-icons-png.flaticon.com/512/5556/5556529.png",
                    speciality: selectedServices,
                    gender:selectedGender,
                    image:image,
                    appointments:[],
                    token:"",
                  });
              }
              firebase.firestore().collection('users')
                .doc(firebase.auth().currentUser.uid)
                .set({
                  firstname,
                  lastname,
                  email,
                  password,
                  typeOfUser,
                });
            }).catch((error) => { alert(error.message) })
        } catch (error) {
          alert(error.message);
        }
      }
      const selectGender = (value)=>{
            setSelectedGender(value);
            if(selectedGender==="Mr"){
                setImage("https://cdn-icons-png.flaticon.com/512/5556/5556468.png")
            }
            else if(selectedGender === "Mrs"){
                setImage("https://cdn.icon-icons.com/icons2/1736/PNG/512/4043251-avatar-female-girl-woman_113291.png")
            }
      }
    return (
        <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={{ flex: 1, backgroundColor: COLORS.white }}>
            <ScrollView
                showsVerticalScrollIndicator={false}
             style={{ flex: 1, marginHorizontal: 22 }}>
                <View style={{ marginVertical: 22 }}>
                    <Text style={{
                        fontSize: 22,
                        fontWeight: 'bold',
                        marginVertical: 12,
                        color: COLORS.black
                    }}>
                        Create Account
                    </Text>

                    <Text style={{
                        fontSize: 16,
                        color: COLORS.black
                    }}>Connect with doctor and patient all around the world 😉</Text>
                </View>

                <View style={{ marginBottom: 12 }}>
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 400,
                        marginVertical: 8
                    }}>Firstname</Text>

                    <View style={{
                        width: "100%",
                        height: 48,
                        borderColor: COLORS.black,
                        borderWidth: 1,
                        borderRadius: 8,
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 22
                    }}>
                        <TextInput
                            placeholder='Enter your firstname'
                            placeholderTextColor={COLORS.black}
                            keyboardType='default'
                            onChangeText={(firstname) => setfirstname(firstname)}
                            style={{
                                width: "95%",
                                fontSize: 15,
                            }}
                        />
                    </View>
                </View>

                <View style={{ marginBottom: 12 }}>
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 400,
                        marginVertical: 8
                    }}>Lastname</Text>

                    <View style={{
                        width: "100%",
                        height: 48,
                        borderColor: COLORS.black,
                        borderWidth: 1,
                        borderRadius: 8,
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 22
                    }}>
                        <TextInput
                            placeholder='Enter your lastname'
                            placeholderTextColor={COLORS.black}
                            keyboardType='default'
                            onChangeText={(lastname) => setLastname(lastname)}
                            style={{
                                width: "95%",
                                fontSize: 15,
                            }}
                        />
                    </View>
                </View>

                <View style={{ marginBottom: 12 }}>
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 400,
                        marginVertical: 8
                    }}>Email address</Text>

                    <View style={{
                        width: "100%",
                        height: 48,
                        borderColor: COLORS.black,
                        borderWidth: 1,
                        borderRadius: 8,
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 22
                    }}>
                        <TextInput
                            placeholder='Enter your email address'
                            placeholderTextColor={COLORS.black}
                            keyboardType='email-address'
                            onChangeText={(email) => setEmail(email)}
                            style={{
                                width: "95%",
                                fontSize: 15,
                            }}
                        />
                    </View>
                </View>

                <View style={{ marginBottom: 12 }}>
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 400,
                        marginVertical: 8
                    }}>Password</Text>

                    <View style={{
                        width: "100%",
                        height: 48,
                        borderColor: COLORS.black,
                        borderWidth: 1,
                        borderRadius: 8,
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 22
                    }}>
                        <TextInput
                            placeholder='Enter your password'
                            placeholderTextColor={COLORS.black}
                            secureTextEntry={isPasswordShown}
                            onChangeText={(password) => setPassword(password)}
                            style={{
                                width: "95%",
                                fontSize: 15,
                            }}
                        />

                        <TouchableOpacity
                            onPress={() => setIsPasswordShown(!isPasswordShown)}
                            style={{
                                position: "absolute",
                                right: 12
                            }}
                        >
                            {
                                isPasswordShown == true ? (
                                    <Ionicons name="eye-off" size={24} color={COLORS.black} />
                                ) : (
                                    <Ionicons name="eye" size={24} color={COLORS.black} />
                                )
                            }

                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <Text style={{ fontSize: 16,  marginHorizontal: 10 , marginBottom:8 }}>Select your gender </Text>
                    <RNPickerSelect
                        value={selectedGender}
                        onValueChange={(itemValue) =>{selectGender(itemValue)}}
                        items={[
                            { label: "Male", value: "Mr" },
                            { label: "Female", value: "Mrs" },
                        ]}
                        style={pickerSelectStyles} // Apply the custom styles
                        placeholder={{
                        label: "Select a gender",
                        value: "",
                        }}
                    />
                </View>
            <View style={{flexDirection: 'row',
                    marginVertical: 6 , justifyContent:'space-between'}}>
                <View style={{
                    flexDirection: 'row',
                    marginVertical: 6
                }}>
                    <Checkbox
                        style={{ marginRight: 8 }}
                        value={isCheckedPatient}
                        onValueChange={setIsCheckedPatient}
                        color={isCheckedPatient ? COLORS.primary : undefined}
                    />

                    <Text>Register as a patient</Text>
                </View>
                <View style={{
                    flexDirection: 'row',
                    marginVertical: 6
                }}>
                    <Checkbox
                        style={{ marginRight: 8 }}
                        value={isChecked}
                        onValueChange={setIsChecked}
                        color={isChecked ? COLORS.primary : undefined}
                    />

                    <Text>Register as a doctor</Text>
                </View>
            </View>
            {isChecked && (
  <View>
    <Text style={{color:"#007260" , fontSize:18 , fontWeight:"400" , marginBottom:8}}>Pick your specialities:</Text>
    {services.map(service => (
      <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 18}} key={service}>
        <Checkbox
          style={{marginRight: 8 , marginBottom:11}}
          value={selectedServices.includes(service)}
          onValueChange={value => handleServiceSelection(value, service)}
          color={COLORS.primary}
        />
        <Text>{service}</Text>
      </View>
    ))}
  </View>
)}

                <Button
                    title="Sign Up"
                    filled
                    style={{
                        marginTop: 18,
                        marginBottom: 4,
                    }}
                    onPress={() => registerUser(email, password, firstname, lastname) }    
                />


                <View style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    marginVertical: 22
                }}>
                    <Text style={{ fontSize: 16, color: COLORS.black }}>Already have an account</Text>
                    <Pressable
                        onPress={() => navigation.navigate("Login")}
                    >
                        <Text style={{
                            fontSize: 16,
                            color: COLORS.primary,
                            fontWeight: "bold",
                            marginLeft: 6
                        }}>Login</Text>
                    </Pressable>
                </View>
            </ScrollView>
         </KeyboardAvoidingView>
    )
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      marginBottom:7,
      fontSize: 16,
      paddingVertical: 12,
      paddingHorizontal: 10,
      borderWidth: 2,
      borderColor: 'gray', // Green border color
      borderRadius: 8,
      color: 'black',
      paddingRight: 30, // To ensure the text is not obscured by the dropdown arrow
    },
    inputAndroid: {
      marginBottom:7,
      fontSize: 16,
      paddingHorizontal: 10,
      paddingVertical: 8,
      borderWidth: 2,
      borderColor: 'gray', // Green border color
      borderRadius: 8,
      color: 'black',
      paddingRight: 30, // To ensure the text is not obscured by the dropdown arrow
    },
  });
export default Signup