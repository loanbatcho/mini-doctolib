import { StyleSheet, Text, View, ScrollView, Pressable, Image } from 'react-native';
import React from 'react';
import { useNavigation } from '@react-navigation/native';

const Services = () => {
  const navigation =useNavigation();
  const services = [
    {
      id: "0",
      image: require('../assets/neurologieService.png'),
      name: "Neurologie",
    },
    {
      id: "1",
      image: require('../assets/pediatricsService.jpg'),
      name: "Pediatrics",
    },
    {
      id: "2",
      image: require('../assets/surgeryService.jpg'),
      name: "Surgery",
    },
    {
      id: "3",
      image: require('../assets/generalService.jpg'),
      name: "General",
    },
    {
      id: "4",
      image: require('../assets/kidney.png'),
      name: "Urologie",
    },
    {
      id: "5",
      image: require('../assets/psy.png'),
      name: "Psychatry",
    },
    {
      id: "6",
      image: require('../assets/family.jpg'),
      name: "Family",
    },
  ];

  return (
    <View style={{ padding: 10 }}>
      <Text style={{ fontSize: 16, fontWeight: "500", marginBottom: 7 }}>Services Available</Text>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {services.map((service, index) => (
          <Pressable onPress={()=>navigation.navigate("Doctors" , {speciality:service.name})} style={{ margin: 10, backgroundColor: "white", padding: 20, borderRadius: 7 }} key={index}>
            <Image source={service.image} style={{ width: 70, height: 70 }} />
            <Text style={{ textAlign: "center", marginTop: 10 }}>{service.name}</Text>
          </Pressable>
        ))}
      </ScrollView>
    </View>
  );
};

export default Services;

const styles = StyleSheet.create({});
