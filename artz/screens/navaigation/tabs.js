import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Appointment from '../Appointment';
// import Doctors from '../../Doctors';
import Homescreen from '../Homescreen';
import { AntDesign } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import { View, Text, Pressable, TextInput, TouchableOpacity  , SafeAreaView,  StyleSheet , Image} from 'react-native'
import SignoutPatients from '../SignoutPatients';
const Tab = createBottomTabNavigator();
 const Tabs = () => {
    return(
        <Tab.Navigator
            screenOptions={
                {
                    "tabBarShowLabel": false,
                    headerShown : false,
                    "tabBarStyle": [
                        { 
                            elevation : 0 , 
                            backgroungColor : "#ffffff",
                            height : 90 , 
                            ...styles.shadow
                        }
                      ]
                }
                        
            }
        >

            <Tab.Screen
                name='Homescreen'
                component={Homescreen}
                options={
                    {
                        tabBarIcon : ({focused}) => (
                            <View style={{ top:10, marginHorizontal: 22 , justifyContent: "center" , alignItems : 'center' }}>         
                                <Ionicons name="home" size={30} color={focused? "#007260" : "#748c94"} />
                                <Text
                                    style={{color:focused? "#007260" : "#748c94", fontSize:focused? 20 : 14}}
                                >
                                    Home
                                </Text>
                            </View>
                            
                        ),
                    }
                }
            
            />
            
            <Tab.Screen
                name='Appointments'
                component={Appointment}
                options={
                    {
                        tabBarIcon : ({focused}) => (
                            <View style={{ top:10, marginHorizontal: 22 , justifyContent: "center" , alignItems : 'center' }}>
                                <Ionicons name="md-calendar" size={30} color={focused? "#007260" : "#748c94"} />
                                <Text
                                    style={{color:focused? "#007260" : "#748c94", fontSize:12}}
                                >
                                    Appointments
                                </Text>
                            </View>
                            
                        )
                    }
                }
            
            />
            
            <Tab.Screen
                name='SignoutPatients'
                component={SignoutPatients}
                options={
                    {
                        tabBarIcon : ({focused}) => (
                            <View style={{ top:10, marginHorizontal: 22 , justifyContent: "center" , alignItems : 'center' }}>
                                <AntDesign name="logout" size={30} color="black" />
                                <Text
                                    style={{color:focused? "#007260" : "#748c94", fontSize:focused? 20 : 14}}
                                >
                                    Out
                                </Text>
                            </View>
                            
                        )
                    }
                }
            
            />

        </Tab.Navigator>
    )
 }


 const styles = StyleSheet.create({
    shadow : {
        shadowColor : '#7F5DF0', 
        shadowOffset : {
            width : 0, 
            height : 10,
        },
        shadowOpacity : 0.252,
        shadowRadius : 3.5,
    }
 })

 export default Tabs