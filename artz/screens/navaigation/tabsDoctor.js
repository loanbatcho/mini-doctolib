import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Signout from '../Signout';
// import Doctors from '../../Doctors';
import Homescreen from '../Homescreen';
import { AntDesign } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import { View, Text, Pressable, TextInput, TouchableOpacity  , SafeAreaView,  StyleSheet , Image} from 'react-native'
import AppointmentsDoctor from '../AppointmentsDoctors';
import {firebase} from '../../config'
import { useRoute } from '@react-navigation/native';
const Tab = createBottomTabNavigator();
 const TabsDoctor = () => {
    const route = useRoute();
    const collection = route.params.collection;
    console.log("Collection;" +collection);
    const currentUser = firebase.auth().currentUser;
    console.log("Current user :" +currentUser)
    return(
        <Tab.Navigator
            
            screenOptions={
                {
                    "tabBarShowLabel": false,
                    headerShown : false,
                    "tabBarStyle": [
                        { 
                            elevation : 0 , 
                            backgroungColor : "#ffffff",
                            height : 90 , 
                            ...styles.shadow
                        }
                      ]
                }          
            }
        >      
            <Tab.Screen
                name='Appointments'
                component={AppointmentsDoctor}
                options={ ({route}) =>({
                        tabBarIcon : ({focused}) => (
                            <View style={{ top:10, marginHorizontal: 22 , justifyContent: "center" , alignItems : 'center' }}>
                                <Ionicons name="md-calendar" size={30} color={focused? "#007260" : "#748c94"} />
                                <Text
                                    style={{color:focused? "#007260" : "#748c94", fontSize:14}}
                                >
                                    Appointments
                                </Text>
                            </View>  
                        ),
                        screenProps: {
                            collection: collection,
                        },
                       })}
            
            />
            <Tab.Screen
                name='Signout'
                component={Signout}
                options={ ({route}) =>({
                        tabBarIcon : ({focused}) => (
                            <View style={{ top:10, marginHorizontal: 22 , justifyContent: "center" , alignItems : 'center' }}>
                                <AntDesign name="logout" size={30} color="black" />
                                <Text
                                    style={{color:focused? "#007260" : "#748c94", fontSize:focused? 20 : 14}}
                                >
                                    Out
                                </Text>
                            </View> 
                        ),
                        screenProps: {
                            col: collection,
                        },
                       })}
            />

        </Tab.Navigator>
    )
 }


 const styles = StyleSheet.create({
    shadow : {
        shadowColor : '#7F5DF0', 
        shadowOffset : {
            width : 0, 
            height : 10,
        },
        shadowOpacity : 0.252,
        shadowRadius : 3.5,
    }
 })

 export default TabsDoctor