import React from 'react';
import { View, Text, Image, StyleSheet , ScrollView } from 'react-native';
import {firebase} from '../config'
import { useEffect, useState } from 'react';
import Button from '../components/Button';
import { useRoute } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
const ProfileScreen = ({ navigation }) => {
    const route = useRoute();
    const collection = (String) (route.params.collection);
    const [userInfo, setUserinfo] = useState('');
    useFocusEffect(
      React.useCallback(() => {
        firebase.firestore().collection(collection)
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());            }
            else{
                console.log('User does not exist');
            }
        })
        console.log('Profile screen refreshed!');
  
        // Return a cleanup function if needed
        return () => {
          // Cleanup code here
        };
      }, [])
    );
    useEffect( ()=> {
        firebase.firestore().collection(collection)
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());            }
            else{
                console.log('User does not exist');
            }
        })
      } , [])
  return (
    <ScrollView style={styles.container}>
      <Image
        source={{ uri: userInfo.image }}
        style={styles.coverImage}
      />
      <View style={styles.avatarContainer}>
        <Image
          source={{ uri: userInfo.image }}
          style={styles.avatar}
        />
        <Text style={[styles.name, styles.textWithShadow]}>{userInfo.firstname} {userInfo.lastname}</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Email:</Text>
          <Text style={styles.infoValue}>{userInfo.email}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Location:</Text>
          <Text style={styles.infoValue}>{userInfo.location}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Bio:</Text>
          <Text style={styles.infoValue}>{userInfo.bio}</Text>
        </View>
      </View>
      <Button
            title="Edit profile"
            filled
            style={{
                marginTop: 18,
                marginBottom: 4,
            }}
            onPress={() => navigation.navigate("EditProfile" , {collection : collection , user:userInfo})}               
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  coverImage: {
    height: 200,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  avatar: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  name: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 30,
    color:'white'
  },
  content: {
    marginTop: 20,
  },
  infoContainer: {
    marginTop: 20,
  },
  infoLabel: {
    fontWeight: 'bold',
  },
  infoValue: {
    marginTop: 5,
  },
});

export default ProfileScreen;