import { View, Text, Image, Pressable, TextInput, TouchableOpacity  , SafeAreaView} from 'react-native'
import { useEffect, useRef, useState } from 'react';
import COLORS from '../constants/colors';
import { Ionicons } from "@expo/vector-icons";
import Checkbox from "expo-checkbox"
import Button from '../components/Button';
import {firebase} from '../config';
import Specialities from './Specialities';
import Search from './Search';
function Dashboard({navigation}){
    const navi = navigation;
    const refGeneral = useRef(null);
    const refFamily = useRef(null);
    const refSurgery = useRef(null);
    const refOptics = useRef(null);
    const refPsy = useRef(null);
    const refPediatry = useRef(null);
    const refEmergency = useRef(null);
    const refUrologie = useRef(null);
    const refNeurologie = useRef(null);

    const [userInfo, setUserinfo] = useState('');
    useEffect( ()=> {
        firebase.firestore().collection('patients')
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());
            }
            else{
                console.log('User does not exist');
            }
        })
      } , [])
    return(
        <SafeAreaView style={{ flex: 1 , justifyContent: "center" , alignItems : 'center' , backgroundColor: COLORS.white }}>
            <View style={{marginTop:10,}}>
                <Text style={{
                            fontWeight: 1000,
                        }}>
                    Hello {userInfo.firstname} {userInfo.lastname} 👋
                </Text>
            </View>
            <Search 
                
                refGeneral={refGeneral}
                refEmergency={refEmergency}
                refFamily={refFamily}
                refNeurologie={refNeurologie}
                refUrologie={refUrologie}
                refSurgery={refSurgery}
                refPsy={refPsy}
                refPediatry={refPediatry}
                refOptics={refOptics}
            />
            <Specialities
                navigation = {navi}
                refGeneral={refGeneral}
                refEmergency={refEmergency}
                refFamily={refFamily}
                refNeurologie={refNeurologie}
                refUrologie={refUrologie}
                refSurgery={refSurgery}
                refPsy={refPsy}
                refPediatry={refPediatry}
                refOptics={refOptics}
            />
        </SafeAreaView>
        
    )
}
export default Dashboard