import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import React from "react";
import LottieView from "lottie-react-native";
import Button from "../components/Button";
import { useNavigation } from "@react-navigation/native";
import {firebase} from '../config';
const Dash = () => {
    const navigation = useNavigation();
    const checkNavigate = async () => {
        try {
          const currentUser = firebase.auth().currentUser;
      
          if (currentUser) {
            // Get the user ID
            const userId = currentUser.uid;
      
            // Retrieve the user document from Firestore
            const userDoc = await firebase.firestore().collection('users').doc(userId).get();
            const typeOfUser = userDoc.data().typeOfUser.toString();
            console.log("User type: ", typeOfUser);
      
            if (typeOfUser === "patients") {
              // Handle navigation for patients
              navigation.navigate("Tabs" ,{collection:"patients"});
            } else if (typeOfUser === "Doctors") {
              // Handle navigation for doctors
              navigation.navigate("TabsDoctor",{collection:"Doctors"});
            }
          }
        } catch (error) {
          console.log("Error:", error);
          // Handle the error
        }
      }
      
  return (
    <SafeAreaView>
      <LottieView
        source={require("../assets/welcome.json")}
        style={{
          height: 360,
          width:300,
          alignSelf: "center",
          marginTop: 40,
          justifyContent: "center",
        }}
        autoPlay
        loop={false}
        speed={0.7}
      />

      <Text
        style={{
          marginTop: 40,
          fontSize: 19,
          fontWeight: "600",
          textAlign: "center",
        }}
      >
        Welcome dear user  ! 
      </Text>
    
    

      <LottieView
        source={require("../assets/sparkle.json")}
        style={{
          height: 300,
          position: "absolute",
          top: 100,
          width: 300,
          alignSelf: "center",
        }}
        autoPlay
        loop={false}
        speed={0.7}
      />
      <Button
            title="Go to home screen"
            filled
            style={{
                marginTop: 18,
                marginBottom: 4,
                padding : 10,
                marginHorizontal:15,
            }}
            onPress={checkNavigate}                   
      />
    </SafeAreaView>
  );
};

export default Dash;

const styles = StyleSheet.create({});