import { View, Text, Image, Pressable, TextInput, TouchableOpacity  , ActivityIndicator, SafeAreaView} from 'react-native'
import { useEffect, useState } from 'react';
import COLORS from '../constants/colors';
import Button from '../components/Button';
import {firebase} from '../config'

function SignoutPatients( ){
    const [userInfo, setUserinfo] = useState('');
    useEffect( ()=> {
        firebase.firestore().collection("patients")
        .doc(firebase.auth().currentUser.uid).get()
        .then( (snapschot) =>{
            if(snapschot.exists){
                setUserinfo(snapschot.data());            }
            else{
                console.log('User does not exist');
            }
        })
      } , [])
    return(
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white , justifyContent: "center" , alignItems : 'center'}}>
            <Text style={{
                        fontWeight: 800,
                    }}>
                🚨 {userInfo.firstname} {userInfo.lastname} your are about to sign out❗ 
            </Text>
            <Button
                    title="Sign out"
                    filled
                    style={{
                        marginTop: 18,
                        marginBottom: 4,
                        padding : 10
                    }}
                    onPress={() => {firebase.auth().signOut()}}                   
                />
        </SafeAreaView>
        
    )
}
export default SignoutPatients