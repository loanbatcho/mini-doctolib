import { useNavigation, useRoute } from '@react-navigation/native';
import { useEffect, useState  , useContext} from 'react';
import { View, Alert, Text, Image, TextInput, StyleSheet, TouchableOpacity , ScrollView  , KeyboardAvoidingView } from 'react-native';
import {firebase} from '../config'
const EditProfileView = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const userInfo = route.params.user;
  const collection = route.params.collection;

  const [firstname, setFirstName] = useState(userInfo.firstname);
  const [lastname, setLastName] = useState(userInfo.lastname);
  const [bio, setBio] = useState(userInfo.bio);
  const [location, setLocation] = useState(userInfo.location);
  const [avatar, setAvatar] = useState(userInfo.image);

  const updateProfile = async (firstname, lastname, bio, location) => {
    try {
      const user = firebase.auth().currentUser;
  
      // Update custom fields in the Firestore database
      await firebase.firestore().collection(collection).doc(user.uid).update({
        firstname:firstname,
        lastname:lastname,
        bio: bio,
        location: location,
      });
      Alert.alert(
        'Information',
        'Profile update successful',
        [
          {
            text: 'Great',
            onPress: () => {
              navigation.navigate("ProfileScreen" , {collection:collection});
            },
          },
        ],
        { cancelable: false }
      );
    
      console.log('Profile updated successfully');
    } catch (error) {
      console.log('Error updating profile:', error);
    }
  };
   
    
  const handleSubmit = () => {
    if (firstname.trim() === '' || lastname.trim() === '' || location.trim() ==='') {
      alert("Only bio can be empty");
      return;
    }
    else{
      Alert.alert(
        'Confirm modification',
        'Are you sure you want to update these informations ? ',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Yes, please',
            onPress: () => {
              updateProfile(firstname, lastname,  bio, location);   
            },
          },
        ],
        { cancelable: false }
      );
    }
  }

  return (
    <KeyboardAvoidingView style={styles.container}>
      <View style={styles.avatarContainer}>
        <Image
          style={styles.avatar}
          source={{uri: avatar}}
        />
        <TouchableOpacity style={styles.changeAvatarButton} onPress={() => {/* open image picker */}}>
          <Text style={styles.changeAvatarButtonText}>Change Avatar</Text>
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.form}>
        <Text style={styles.label}>Firstname</Text>
        <TextInput
          style={styles.input}
          placeholder="Enter firstname"
          value={firstname}
          onChangeText={setFirstName}
        />
        <Text style={styles.label}>Lastname</Text>
        <TextInput
          style={styles.input}
          placeholder="Enter lastname"
          value={lastname}
          onChangeText={setLastName}
        />
        
        <Text style={styles.label}>Location</Text>
        <TextInput
          style={styles.input}
          placeholder="Enter Location"
          value={location}
          onChangeText={setLocation}
        />
        <Text style={styles.label}>Bio</Text>
        <TextInput
          style={styles.input}
          placeholder="Enter Bio"
          value={bio}
          onChangeText={setBio}
        />
        <TouchableOpacity style={styles.button} onPress={handleSubmit}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    width: '80%',
  },
  label: {
    marginTop: 20,
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    fontSize: 18,
  },
  button: {
    marginTop: 20,
    backgroundColor: "#007260",
    borderRadius: 15,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  buttonTry:{
    flex : 1,
    flexDirection:"row",
    justifyContent: "space-between", 
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
  },
  avatarContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  changeAvatarButton: {
    marginTop: 10,
  },
  changeAvatarButtonText: {
    color: "#007260",
    fontSize: 18,
  },
});

export default EditProfileView;